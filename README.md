# Angular Data Table

A basic implementation of an editable html table that can handle different datasource types and column definitions.  
The solution also supports replacing the custom table with an already existing solution.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.3.0.  
Run ` npm install` for installing dependencies.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## What's this about?

This small demo application intends to show different approaches for creating a reusable and editable table.

The project tries to show different ways for solving general problems. These can be, for example, of a programmatic or architectural nature. A fully responsive and good looking design wasn't the goal of this solution. To make it look a bit better Bootstrap was used. Next to AG Grid (see Quick and Easy) this project only uses lodash as additional library. And, of course, eslint and prettier for linting and code formatting. The rest of the project uses the default settings set on creation.

## What does it do?

There are 2 different solutions presented on 3 different pages, Users, Products and QuickAndEasy. Users and Products both use the table implementation of this project. QuickAndEasy demonstrates how the general logic can be easily adapted for usage with third-party libraries. In this case AG Grid.

### Users

The table in this example is configured to use local data. Necessary data is prefetched and passed as input parameter to the table. For demo purposes a table search event will always produce a http request. Therefore an API endpoint is also provided to the table. Otherwise this wouldn't be necessary.

### Products

In this example the table is configured to use remote data. Therefore the table handles initial data loading itself. Since the API used for fetching products doesn't support most of the endpoints needed, only sorting will be performed as remote operation. Unfortunately the api sorts by ID per default, so only the sort direction has an effect. But the basic idea should be clear.

### Quick And Easy

This example demonstrates how the general logic, for configuring the table's datasource and visible columns, can be used with an external library. In this case it's AG Grid but it could be any other library.

## What could be next?

Given the current scenario listing all the things that could come next would be over the top. But there are some things worth to be mentioned.

### Project structure

This is a very small and simple project. But imagining this growing to a component library different changes can be considered.

- A feature or domain oriented folder structure
- Using nx workspace and libraries for structuring the code
- Creating an actual Angular library or libraries

### Table configuration

The interface for the table configuration could be extended in many ways.

- Extend column definition and make it more configurable. For example adding sortable: boolean property.
- Adding a `Hybrid` datasource type for loading data remotely and manage locally. Create a clear distinction between local, remote and mixed data handling.

### API Service

The question here is how the general architecture should look like in the future.  
For a generic approach like this, the service could implement basic CRUD functionality.
For handling different endpoints and parameters an adapter for each API could be implemented.  
Another approach could be an interface defining basic CRUD operations. For each API a service implementing that interface could be created, and only the interface gets passed to the table.

### Sorting

Right now sorting only supports single column selection. By having a parent sorter directive in place, handling multi column sorting can be achieved easily.

### Editing

In the end this depends on how the user wants to interact with the table. Having buttons makes it more usable on mobile devices like tablets. But things like double-click for entering edit mode are also possible. Using column editing instead of row editing could be another way. Column editing for example could be achieved by creating a structural directive that switches between different templates.

A master/detail solution or an edit popup would also be possible. Good thing is, no matter which way was chosen the logic for a dynamic form could always be reused.

The logic for dynamic form creation could be extended by adding validation rules based on column definitions. If this logic growes it should be implemented in a dedicated service or something similar.

### State persistence

Given the flexibility and possible features, the table probably will need some kind of state persistence. This way the user would have the possibility to save different sort and filter combinations for easy and quick access. In addition restoring state after navigation would be possible. Could be needed in master/detail scenarios.

## Further help

Passing different configurations and api endpoints to the table during runtime would also work but wasn't implemented.  
Instead the table configuration in one of the page components can be adjusted.
