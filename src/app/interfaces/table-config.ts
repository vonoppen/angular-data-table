/** The available types for table datasources. */
export type DataSourceType = 'Local' | 'Remote';

/**
 * A general definition for table columns.
 * The key is used as identifier and the
 * name as column header text. If name is not present
 * key is used as column header.
 */
export interface ColDef {
  key: string;
  name?: string;
}

/**
 * Configuration object for the `CustomTable`.
 * Since dataSourceType can be local, apiEndpoint is optional.
 * The rest is mandatory and needed by the table for initialization.
 */
export interface TableConfig {
  apiEndpoint?: string;
  columns: Array<ColDef>;
  dataIdentifier: string;
  dataSourceType: DataSourceType;
  initialPageSize: number;
}
