/**
 * Event definitions for table related operations
 */

/**
 * Informations passed with a `SortChange` event
 */
export interface SortChangeEvent {
  column: string;
  direction: 'asc' | 'desc' | null;
}

/**
 * Informations passed with a `Search` event
 */
export interface SearchEvent {
  query: string;
}

/**
 * Informations passed with a `PageChange` event
 */
export interface PageChangeEvent {
  page: number;
}

/**
 * Informations passed with a `PageSizeChange` event
 */
export interface PageSizeChangeEvent {
  pageSize: number;
}
