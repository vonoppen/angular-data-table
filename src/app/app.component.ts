import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavigationComponent } from './components';

/**
 * Demo App application component.
 *
 * The `AppComponent` is the entrypoint for this application.
 * It renders the basic html structure including the global `NavigationComponent`
 * and the `router-outlet`.
 *
 */
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [NavigationComponent, RouterOutlet],
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'table-demo-app';
}
