import { Routes } from '@angular/router';
import {
  HomeComponent,
  NotFoundComponent,
  ProductsComponent,
  QuickAndEasyComponent,
  UsersComponent,
} from './pages';

/**
 * Application routes for all pages displaying a table.
 * Additional home page (which wouldn't be necessary).
 * Redirect to home page for empty path.
 * Wildcard route for catching everything else.
 */
export const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'users',
    component: UsersComponent,
  },
  {
    path: 'products',
    component: ProductsComponent,
  },
  {
    path: 'quickandeasy',
    component: QuickAndEasyComponent,
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];
