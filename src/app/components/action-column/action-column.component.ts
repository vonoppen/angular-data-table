import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

/**
 * This component is a simple template displaying action buttons.
 * Depending on the current mode (display/edit) different buttons are displayed.
 */
@Component({
  standalone: true,
  templateUrl: './action-column.component.html',
})
export class ActionColumnComponent implements ICellRendererAngularComp {
  private params!: ICellRendererParams;

  isEditing: boolean = false;

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

  refresh(): boolean {
    return true;
  }

  onEdit(): void {
    this.isEditing = true;

    this.params.api.startEditingCell({
      rowIndex: this.params.node.rowIndex as number,
      colKey: this.params.api.getDisplayedCenterColumns()[0].getColId(),
    });
  }

  onUpdate() {
    this.isEditing = false;
    this.params.api.stopEditing();
  }

  onCancel() {
    this.isEditing = false;
    this.params.api.stopEditing(true);
  }
}
