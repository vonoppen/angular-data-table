import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  computed,
  Input,
  input,
  OnChanges,
  signal,
  Signal,
  SimpleChanges,
  WritableSignal,
} from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { get, orderBy } from 'lodash';
import {
  ColDef,
  PageChangeEvent,
  PageSizeChangeEvent,
  SearchEvent,
  SortChangeEvent,
  TableConfig,
} from '../../interfaces';
import { SortableDirective, SorterDirective } from '../../directives';
import { ApiService } from '../../services/api.service';
import { SearchBarComponent } from '../search-bar/search-bar.component';
import { CustomTableFooterComponent } from '../custom-table-footer/custom-table-footer.component';

/**
 * Demo App custom table component.
 *
 * This `CustomTableComponent` represents the actual table. Searchbar and footer components
 * are imported and rendered above/under the table.
 * Sort directives are applied to the <table> and the <th> elements.
 * For row based editing the `displayRow` and `editRow` templates are used to switch between the
 * different states.
 * The edit form is a reactive form dynamically built based on the current data model using
 * `FormBuilder`.
 * The table subscribes to all event emitters provided by the footer, searchbar and the sort
 * directive. Depending on the current datasource tpye these events then will be handled
 * accordingly.
 */
@Component({
  selector: 'app-custom-table',
  standalone: true,
  imports: [
    CustomTableFooterComponent,
    NgTemplateOutlet,
    ReactiveFormsModule,
    SearchBarComponent,
    SortableDirective,
    SorterDirective,
  ],
  templateUrl: './custom-table.component.html',
  styleUrl: './custom-table.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomTableComponent<T> implements AfterViewInit, OnChanges {
  // Optional input for table data in case of local datasource.
  @Input() tableData?: Array<T>;

  // Input using signal based inputs for demonstration purpose
  tableConfig = input.required<TableConfig>();

  // Update table headers when table config reference changes
  tableHeaders: Signal<Array<ColDef>> = computed(
    () => this.tableConfig().columns
  );

  // Update the list of available fields associated with the current generic type.
  tableDataKeys: Signal<Array<keyof T>> = computed(() =>
    this.tableConfig().columns.map((def: ColDef) => def.key as keyof T)
  );

  // The actual data array used by the table also using signals
  rowData: WritableSignal<Array<T>>;
  // The current row marked for editing.
  // Used in view template for switching between row templates
  activeEditRow: number;
  // The edit form used in `editRow` template
  form: FormGroup;

  // Internal data array holding either remote or local data.
  private dataSet: Array<T>;
  private currentPage: number;
  private currentPageSize: number;
  private mode: 'Local' | 'Remote';

  constructor(
    private api: ApiService<T>,
    private fb: FormBuilder
  ) {
    this.rowData = signal(new Array<T>());
    this.dataSet = [];
    this.currentPage = 1;
    this.currentPageSize = 10;
    this.activeEditRow = -1;
    this.mode = 'Local';

    this.form = new FormGroup({});
  }

  // Getter returning the current number of total entries
  get dataSize(): number {
    return this.dataSet.length;
  }

  /**
   * Update current mode and page size passed by the table configuration.
   * If mode is `Remote` the injected api service is used to request data
   * and assign it to the local dataset. Afterwards the row data will be updated.
   */
  ngAfterViewInit(): void {
    this.mode = this.tableConfig().dataSourceType;

    if (this.mode === 'Remote') {
      this.api.getAll(this.tableConfig().apiEndpoint!).subscribe(res => {
        this.dataSet = res;
        this.calculateVisibleRows(this.tableConfig().initialPageSize);
      });
    }

    this.currentPageSize = this.tableConfig().initialPageSize;
  }

  /**
   * Listen to changes on the tableData input property.
   * If a value is present it will be set as local data
   * and a row data update will be initiated.
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['tableData']?.currentValue) {
      this.dataSet = changes['tableData'].currentValue;
      this.calculateVisibleRows(this.currentPageSize);
    }
  }

  /**
   * Listener for search event emitted by `SearchBarComponent`.
   * For demo purposes the event triggers a data load.
   * The resulting data set then gets filtered based on the given search query.
   * The filter logic is very basic and only fits the current needs.
   * @param event Search event from searchbar
   */
  onTableSearch(event: SearchEvent): void {
    this.api.getAll(this.tableConfig().apiEndpoint!).subscribe(res => {
      let data = res;

      if (event.query.length > 0) {
        data = res.filter((data: T) => {
          return Object.values(data as { [s: string]: unknown }).find(value => {
            let strValue: string;

            switch (typeof value) {
              case 'number':
                strValue = String(value);
                break;
              case 'string':
                strValue = value;
                break;
              default:
                strValue = '';
                break;
            }

            return strValue.toLowerCase().includes(event.query.toLowerCase());
          });
        });
      }

      this.dataSet = data;
      this.calculateVisibleRows(this.currentPageSize);
    });
  }

  /**
   * Listener for page change event emitted by `CustomTableFooterComponent`.
   * @param event
   */
  onPageChanged(event: PageChangeEvent): void {
    this.currentPage = event.page;
    this.calculateVisibleRows(this.currentPageSize);
  }

  /**
   * Listener for page size change event emitted by `CustomTableFooterComponent`.
   * @param event
   */
  onPageSizeChanged(event: PageSizeChangeEvent): void {
    this.currentPageSize = event.pageSize;
    this.calculateVisibleRows(event.pageSize);
  }

  /**
   * Listener for sort change event emitted by `SearchBarComponent`.
   * For demo purposes sorting depends on the current mode, Local or Remote.
   * If mode is Local, lodash is used to sort the local dataset by the provided
   * column and direction.
   * If mode is Remote, sorting will be handled on the server. Column and direction
   * will be sent as url query.
   * @param event
   */
  onSortChange(event: SortChangeEvent) {
    if (event.direction) {
      if (this.mode === 'Local') {
        const ordered = orderBy(
          this.dataSet,
          [event.column],
          [event.direction!]
        );
        this.dataSet = ordered;
        this.calculateVisibleRows(this.currentPageSize);
      } else {
        this.api
          .getSortedByKey(
            this.tableConfig().apiEndpoint!,
            event.column,
            event.direction
          )
          .subscribe(res => {
            this.dataSet = res;
            this.calculateVisibleRows(this.currentPageSize);
          });
      }
    }
  }

  /**
   * Click listener for row edit button.
   * Sets activeEditRow and initiates form built.
   * @param idx Row index
   */
  onEditRow(idx: number): void {
    this.buildForm(this.rowData()[idx]);
    this.activeEditRow = idx;
  }

  /**
   * Click listener for cancel edit button.
   * Resets form and activeEditRow.
   */
  onCancelEdit(): void {
    this.form = new FormGroup({});
    this.activeEditRow = -1;
  }

  /**
   * Click listener for row update button.
   * Both, local and table, dataset are updated by replacing
   * the updated data entry.
   */

  onUpdateRow(): void {
    const idKey = this.tableConfig().dataIdentifier as keyof T;
    const idx = this.dataSet.findIndex(
      data => data[idKey] === this.form.get(idKey.toString())?.value
    );

    this.dataSet.splice(idx, 1, this.form.value);

    const data = this.rowData().map(data => {
      return data[idKey] === this.form.get(idKey.toString())?.value
        ? this.form.value
        : data;
    });

    this.onCancelEdit();
    this.rowData.set(data);
  }

  /**
   * A helper method for getting the value of a nested
   * object property.
   * The method uses lodash for reading the value.
   * For now the value will just be transformed to a string.
   * In the future this could also include formatters (data, number, ...)
   * @param key
   * @param data
   * @returns
   */
  getPropertyValue(key: keyof T, data: T): string {
    if (key.toString().includes('.')) return get(data, key);

    return '' + data[key];
  }

  /**
   * This method simply uses the current page and page size
   * for calculating a start and end index.
   * Via slice and array with only visible entries gets created
   * and set as row data.
   * @param pageSize
   */
  private calculateVisibleRows(pageSize: number): void {
    const start = (this.currentPage - 1) * pageSize;
    const end = this.currentPage * pageSize;
    const visibleRows = this.dataSet.slice(start, end);

    this.rowData.set(visibleRows);
  }

  /**
   * This method is used to dynamically build a reactive form.
   * Based on the current generic type a formControl for each
   * available property will be created.
   * This solution only serves the current needs and is just the beginning.
   * For starters making it a recursive function might be the more elegant way.
   * Support for additional types would be good. And then there are validators.
   * The list goes on.
   * @param data
   */
  private buildForm(data: T) {
    const keys = Object.keys(data as object);
    const rootGroup = this.fb.group({});

    for (const key of keys) {
      if (typeof data[key as keyof T] === 'object') {
        const group = this.createNestedFormGroup(
          data[key as keyof T] as Record<string, unknown>
        );
        rootGroup.addControl(key, group);
      } else {
        const control = this.fb.control(data[key as keyof T]);
        rootGroup.addControl(key, control);
      }
    }

    this.form = rootGroup;
  }

  /**
   * Helper for creating nested form controls.
   * @param data
   * @returns
   */
  private createNestedFormGroup(data: Record<string, unknown>): FormGroup {
    const group = this.fb.group({});
    const keys = Object.keys(data as object);

    for (const key of keys) {
      const control = this.fb.control(data[key]);
      group.addControl(key, control);
    }

    return group;
  }
}
