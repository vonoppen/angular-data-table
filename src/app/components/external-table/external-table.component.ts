import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
  Signal,
} from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { AgGridAngular } from 'ag-grid-angular';
import { ColDef, GridApi, GridReadyEvent } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { ColDef as CustomColDef, TableConfig } from '../../interfaces';
import { ActionColumnComponent } from '../../components/action-column/action-column.component';
import { ApiService } from '../../services/api.service';

/**
 * This component displays a ready to use AG Grid table.
 * It is based on the same principles regarding flexibilty as the custom table implementation.
 * Basic configuration will be passed as input parameter and adapted to work with AG Grid.
 * In this use case AG Grid handles all the heavy lifting except the initial data loading.
 */
@Component({
  selector: 'app-external-table',
  standalone: true,
  imports: [AgGridAngular, AsyncPipe],
  templateUrl: './external-table.component.html',
  styleUrl: 'external-table.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExternalTableComponent<T> {
  tableConfig = input.required<TableConfig>();

  colDefs: Signal<ColDef[]> = computed(() => {
    const colDefs: Array<ColDef> = this.tableConfig().columns.map(
      (def: CustomColDef) => {
        return { field: def.key, headerName: def.name } as ColDef;
      }
    );

    colDefs.push({
      headerName: 'Actions',
      minWidth: 150,
      cellRenderer: ActionColumnComponent,
      editable: false,
      colId: 'action',
      resizable: false,
      sortable: false,
    });

    return colDefs;
  });

  defaultColDef = {
    flex: 1,
    editable: true,
  };

  rowData?: Observable<Array<T>>;

  private gridApi!: GridApi;

  constructor(private api: ApiService<T>) {}

  onGridReady(params: GridReadyEvent) {
    this.gridApi = params.api;
    this.rowData = this.api.getAll(this.tableConfig().apiEndpoint!);
  }

  onSearch(query: string) {
    this.gridApi.setGridOption('quickFilterText', query);
  }
}
