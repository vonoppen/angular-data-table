import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { PageChangeEvent, PageSizeChangeEvent } from '../../interfaces';

/**
 * A table footer component that can be attached to a table.
 *
 * This `CustomTableFooterComponent` currently renders the pagination part
 * of the table. When the page or the page size change, a corresponding event
 * will be emitted.
 * The initial page size and the number of total entries will be passed as input
 * parameters.
 */
@Component({
  selector: 'app-custom-table-footer',
  standalone: true,
  templateUrl: './custom-table-footer.component.html',
  styleUrl: './custom-table-footer.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomTableFooterComponent {
  @Input({ required: true })
  set initialPageSize(size: number) {
    this.pageSize = size;
  }

  // Calculate total pages when total number of entries changes
  @Input({ required: true })
  set totalEntries(total: number) {
    this._numOfEntries = total;
    this.setTotalPages();
  }

  @Output() pageChanged: EventEmitter<PageChangeEvent> = new EventEmitter();
  @Output() pageSizeChanged: EventEmitter<PageSizeChangeEvent> =
    new EventEmitter();

  currentPage = 1;
  pageSize = 10;
  totalPages = 10;

  private _numOfEntries = 0;

  /**
   * Change listener for page size select element.
   * Updates current page and emits page change event.
   * @param page The new page.
   */
  changePage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
      this.pageChanged.emit({ page });
    }
  }

  /**
   * Click listener for prev/next page buttons.
   * Updates current page size and total pages.
   * Resets current page to 1.
   * @param size The new page size.
   */
  changePageSize(size: string): void {
    this.pageSize = +size;
    this.changePage(1);
    this.setTotalPages();

    this.pageSizeChanged.emit({ pageSize: +size });
  }

  /**
   * Calculate number of total pages.
   */
  private setTotalPages(): void {
    this.totalPages = Math.ceil(this._numOfEntries / this.pageSize);
  }
}
