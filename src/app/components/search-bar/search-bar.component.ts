import { Component, EventEmitter, Output } from '@angular/core';
import { SearchEvent } from '../../interfaces';

/**
 * A searchbar component that can be attached to a table.
 *
 * This very simple searchbar currently provides an input field and a button.
 * When the button is clicked a search event with the current search query as
 * value will be emitted.
 * The search then can be handled elsewhere.
 */
@Component({
  selector: 'app-search-bar',
  standalone: true,
  imports: [],
  templateUrl: './search-bar.component.html',
})
export class SearchBarComponent {
  @Output() searchEvent = new EventEmitter<SearchEvent>();

  onSearch(query: string): void {
    this.searchEvent.emit({ query });
  }
}
