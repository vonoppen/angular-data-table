import { Directive, EventEmitter, Output } from '@angular/core';
import { SortChangeEvent } from '../interfaces';

/**
 * A directive for handling table sorting.
 *
 * This directive is responsible for tracking the current sorting state.
 * It acts as a parent directive for the `Sortable` directive.
 * When a sort event gets triggered, the new state will be determined
 * and emitted as `SortChangeEvent`.
 */
@Directive({
  selector: '[appSorter]',
  standalone: true,
})
export class SorterDirective {
  /** EventEmitter for SortChange events. */
  @Output() sortChange = new EventEmitter<SortChangeEvent>();

  // The currently selected column
  active: string | null = null;
  // The current sort direction, or null if no sorting
  direction: 'asc' | 'desc' | null = null;

  /**
   * Sets the new active column and sorting direction.
   * Emits changes to subscribers.
   * @param column The column to be sorted by.
   */
  sort(column: string) {
    let direction = this.direction;

    // If column is not the same as active, reset the direction
    if (this.active !== column) {
      this.direction = null;
      this.active = column;
    }

    switch (this.direction) {
      case 'asc':
        direction = 'desc';
        break;
      case 'desc':
        direction = null;
        break;
      default:
        direction = 'asc';
        break;
    }
    // Emit the current active column and the direction
    this.sortChange.emit({
      column,
      direction,
    });

    this.direction = direction;
  }
}
