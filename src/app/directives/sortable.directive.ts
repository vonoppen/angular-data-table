import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostListener,
  OnDestroy,
} from '@angular/core';
import { SorterDirective } from './sorter.directive';
import { SortChangeEvent } from '../interfaces';
import { Subscription } from 'rxjs';

/**
 * A directive for making a table header column clickable
 * and triggering a sort event.
 *
 * This directive adds a click listener to its host element.
 * The listener then triggers a sort event, which will be processed
 * by the injected `Sorter` directive.
 * By subscribing to the Sorter's change event each instance can
 * update itself when the overall state changes.
 * This way each sortable column handles its own state what makes
 * it more flexible.
 */
@Directive({
  selector: '[appSortable]',
  standalone: true,
})
export class SortableDirective implements AfterViewInit, OnDestroy {
  // The host's css class list
  private classList: DOMTokenList;
  // The model's property bound to this column.
  private ref: string;
  // Subscription to sort change event
  private sub: Subscription;

  constructor(
    private el: ElementRef,
    private sorter: SorterDirective
  ) {
    this.ref = '';
    this.classList = this.el.nativeElement.classList;

    // Subscribe to sort change event
    this.sub = this.sorter.sortChange.asObservable().subscribe(change => {
      this.updateStatus(change);
    });

    // CSS class adding an arrow to the host element.
    this.classList.add('arrow');
  }

  @HostListener('click') onHeaderCLicked() {
    this.sorter.sort(this.ref);
  }

  ngAfterViewInit(): void {
    this.ref = this.el.nativeElement.getAttribute('data-key');
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  /**
   * Callback function for sort change events.
   * Updates the css class for toggling the direction arrow.
   * @param event Event emitting the new sort state.
   */
  private updateStatus(event: SortChangeEvent): void {
    this.classList.remove('asc', 'desc');

    if (event.column == this.ref && event.direction)
      this.classList.add(event.direction);
  }
}
