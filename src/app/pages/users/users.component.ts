import { Component } from '@angular/core';
import { TableConfig } from '../../interfaces';
import { CustomTableComponent } from '../../components';
import { ApiService } from '../../services/api.service';

/**
 * Demo App users component.
 *
 * The `UsersComponent` shows a list of users by using the `CustomTableComponent`.
 */
@Component({
  selector: 'app-users',
  standalone: true,
  imports: [CustomTableComponent],
  templateUrl: './users.component.html',
})
export class UsersComponent<User> {
  /** API url for loading products */
  private readonly apiEndpoint = 'https://jsonplaceholder.typicode.com/users';

  /** Configuration object passed to custom table as input parameter */
  tableConfig: TableConfig;
  /** An array with user objects passed to custom table as input paramter */
  tableData?: Array<User>;

  /**
   * Initialize table configuration for handling users.
   * Load users using `ApiService` and set as table data.
   * @param api Injected ApiService for preloading data.
   */
  constructor(private api: ApiService<User>) {
    this.tableConfig = {
      dataSourceType: 'Local',
      apiEndpoint: this.apiEndpoint,
      columns: [
        { key: 'id', name: 'ID' },
        { key: 'name', name: 'Name' },
        { key: 'username', name: 'Username' },
        { key: 'email', name: 'Email' },
        { key: 'phone', name: 'Phone' },
        { key: 'website', name: 'Website' },
        { key: 'address.street', name: 'Street' },
      ],
      initialPageSize: 5,
      dataIdentifier: 'id',
    } as TableConfig;

    this.api.getAll(this.apiEndpoint).subscribe(res => {
      this.tableData = res;
    });
  }
}
