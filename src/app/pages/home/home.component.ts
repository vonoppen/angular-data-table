import { Component } from '@angular/core';
/**
 * Demo App home component.
 *
 * The `HomeComponent` is the landingpage for this application.
 */
@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  templateUrl: './home.component.html',
  styleUrl: 'home.component.scss',
})
export class HomeComponent {}
