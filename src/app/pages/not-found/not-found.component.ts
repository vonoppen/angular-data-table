import { Component } from '@angular/core';
/**
 * Demo App notFound component.
 *
 * The `NotFoundComponent` will be displayed everytime a user
 * tries to access a path that has no routing definition.
 */

@Component({
  selector: 'app-not-found',
  standalone: true,
  imports: [],
  templateUrl: './not-found.component.html',
})
export class NotFoundComponent {}
