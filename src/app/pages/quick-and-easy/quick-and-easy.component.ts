import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { AgGridAngular } from 'ag-grid-angular';
import { TableConfig } from '../../interfaces';
import { ExternalTableComponent } from '../../components';

/**
 * Demo App quick and easy component.
 *
 * The `QuickAndEasyComponent` shows how the flexibility, regarding
 * table data and column definition, can also be used in combination
 * with a ready to use third-party library like `AG Grid`.
 * .
 */

@Component({
  selector: 'app-quick-and-easy',
  standalone: true,
  imports: [AgGridAngular, AsyncPipe, ExternalTableComponent],
  templateUrl: './quick-and-easy.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuickAndEasyComponent {
  /** API url for loading users */
  private readonly apiEndpoint = 'https://jsonplaceholder.typicode.com/users';

  /** Configuration object passed to custom table as input parameter */
  config: TableConfig;

  /**
   * Initialize table configuration for handling users.
   * This is a minimal setup. The rest is handled by AG Grid
   * default settings, which also can be overridden.
   */
  constructor() {
    this.config = {
      dataSourceType: 'Remote',
      apiEndpoint: this.apiEndpoint,
      columns: [
        { key: 'id' },
        { key: 'name' },
        { key: 'username' },
        { key: 'email' },
        { key: 'phone' },
        { key: 'website' },
      ],
    } as TableConfig;
  }
}
