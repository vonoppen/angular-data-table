import { Component } from '@angular/core';
import { TableConfig } from '../../interfaces';
import { CustomTableComponent } from '../../components';

/**
 * Demo App products component.
 *
 * The `ProductsComponent` shows a list of products by using the `CustomTableComponent`.
 */
@Component({
  selector: 'app-products',
  standalone: true,
  imports: [CustomTableComponent],
  templateUrl: './products.component.html',
})
export class ProductsComponent {
  /** API url for loading products */
  private readonly apiEndpoint = 'https://fakestoreapi.com/products';

  /** Configuration object passed to custom table as input parameter */
  tableConfig: TableConfig;

  /**
   * Initialize table configuration for handling products.
   */
  constructor() {
    this.tableConfig = {
      dataSourceType: 'Remote',
      apiEndpoint: this.apiEndpoint,
      columns: [
        { key: 'id', name: 'ID' },
        { key: 'title', name: 'Title' },
        { key: 'price', name: 'Price' },
        { key: 'category', name: 'Category' },
        { key: 'description', name: 'Description' },
        // { key: 'image', name: 'Image' },
      ],
      initialPageSize: 5,
      dataIdentifier: 'id',
    } as TableConfig;
  }
}
