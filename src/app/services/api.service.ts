import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
 * Demo App http service.
 *
 * A generic service class implementing various JSON API endpoints.
 *
 * The main intention here is to have one single service for handling requests to any
 * json api. This means handling different entities, which is why a generic type is used here.
 * Instead of explicitly assigning a certain entity/model type as return type, the generic type is
 * used.
 * This way the consumer is responsible for providing a type, not the service.
 *
 * Currently only methods for requesting either all data or a sorted dataset
 * are implemented. More functionality can be added at any time.
 */
@Injectable({
  providedIn: 'root',
})
export class ApiService<T> {
  constructor(private http: HttpClient) {}

  /**
   * This method performs a GET request
   * for laoding all data from a given endpoint,
   * and returns an observable for further processing
   * @param apiEndpoint
   * @returns Observable<Array<T>>
   */
  getAll(apiEndpoint: string): Observable<Array<T>> {
    return this.http.get<Array<T>>(apiEndpoint);
  }

  /**
   * This method performs a GET request
   * for loading a sorted dataset form a given endpoint,
   * and returns an observable for further processing.
   * The endpoints used in this example don't support sorting by key,
   * so only the direction paramter is used.
   * @param apiEndpoint
   * @param _key
   * @param direction
   * @returns Observable<Array<T>>
   */
  getSortedByKey(
    apiEndpoint: string,
    _key: string,
    direction = 'asc'
  ): Observable<Array<T>> {
    return this.http.get<Array<T>>(`${apiEndpoint}?sort=${direction}`);
  }
}
